<?php

/**
 * Implements hook_views_bulk_operations_form_alter().
 *
 * Tweaks the appearance of the VBO selector.
 * Source - https://drupal.org/project/shiny
 */
function glance_views_bulk_operations_form_alter(&$form, &$form_state, $vbo) {
  if ($form_state['step'] == 'views_form_views_form') {
    $form['select']['#title'] = '';
    $form['select']['#collapsible'] = FALSE;
    $form['select']['submit']['#value'] = t('Apply');
    $form['select']['operation']['#options'][0] = t('Bulk operations');
    $form['select']['#weight'] = 99999;
  }
}

/**
 * Override or insert variables into the html template.
 * @link https://drupal.org/node/2014851 @endlink
 * Issue #2014851 explains the core bug causing the font css import workaround.
 * Source - https://drupal.org/project/shiny
 */
function glance_preprocess_html(&$vars) {
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 9', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));
  // Add conditional CSS for IE8 and below.
  drupal_add_css(path_to_theme() . '/css/ie8.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));
  // Add conditional CSS for IE7 and below.
  drupal_add_css(path_to_theme() . '/css/ie7.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));
  // Add conditional CSS for IE6.
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 6', '!IE' => FALSE), 'weight' => 999, 'preprocess' => FALSE));
  // Add external CSS for Open Sans from Google Fonts.
  drupal_add_css('//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&subset=latin,latin-ext',array('type' => 'external'));
	
	$tooltips = theme_get_setting('glance_tooltips');
	if($tooltips){
		drupal_add_js(drupal_get_path('theme', 'glance') . '/js/jquery.qtip.min.js');
		drupal_add_js(drupal_get_path('theme', 'glance') . '/js/tooltips.js');
		drupal_add_css(drupal_get_path('theme', 'glance') . '/css/jquery.qtip.css');
	}
	
}

/**
 * Override or insert variables into the page template.
 * Source - https://drupal.org/project/shiny
 */
function glance_preprocess_page(&$vars) {
  $vars['primary_local_tasks'] = $vars['tabs'];
  unset($vars['primary_local_tasks']['#secondary']);
  $vars['secondary_local_tasks'] = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => $vars['tabs']['#secondary'],
  );

  if (module_exists('overlay')) {
    if (overlay_get_mode() == 'child') {
      $vars['breadcrumb'] = '';
    }
  }
	
  // Set a page icon class.
  $vars['page_icon_class'] = ($item = menu_get_item()) ? implode(' ' , _glance_icon_classes($item['href'])) : '';
	
}

function glance_css_alter(&$css) {

  $exclude = array(
  // 'modules/aggregator/aggregator.css' => FALSE,
//   'modules/block/block.css' => FALSE,
//   'modules/book/book.css' => FALSE,
//   'modules/comment/comment.css' => FALSE,
//   'modules/dblog/dblog.css' => FALSE,
//   'modules/field/theme/field.css' => FALSE,
//   'modules/file/file.css' => FALSE,
//   'modules/filter/filter.css' => FALSE,
//   'modules/forum/forum.css' => FALSE,
//   'modules/help/help.css' => FALSE,
//   'modules/menu/menu.css' => FALSE,
//   'modules/node/node.css' => FALSE,
//   'modules/openid/openid.css' => FALSE,
   'modules/poll/poll.css' => FALSE,
//   'modules/profile/profile.css' => FALSE,
//   'modules/search/search.css' => FALSE,
//   'modules/statistics/statistics.css' => FALSE,
//   'modules/syslog/syslog.css' => FALSE,
//   'modules/system/admin.css' => FALSE,
//   'modules/system/maintenance.css' => FALSE,
//   'modules/system/system.css' => FALSE,
   'modules/system/system.admin.css' => FALSE,
//   'modules/system/system.base.css' => FALSE,
//   'modules/system/system.maintenance.css' => FALSE,
   'modules/system/system.messages.css' => FALSE,
//   'modules/system/system.menus.css' => FALSE,
//   'modules/system/system.theme.css' => FALSE,
//   'modules/taxonomy/taxonomy.css' => FALSE,
//   'modules/tracker/tracker.css' => FALSE,
   'modules/overlay/overlay-child.css' => FALSE,
   'modules/overlay/overlay-parent.css' => FALSE,
   'modules/dasboard/dashboard.css' => FALSE,
   'modules/update/update.css' => FALSE,
//   'modules/user/user.css' => FALSE,
  'misc/vertical-tabs.css' => FALSE,
  'misc/ui/jquery.ui.theme.css' => FALSE,

  // Remove contrib module CSS
  drupal_get_path('module', 'views') . '/css/views-admin.css' => FALSE,
  drupal_get_path('module', 'views') . '/css/views-admin.theme.css' => FALSE,
  drupal_get_path('module', 'views') . '/css/views-admin.ctools.css' => FALSE,
  drupal_get_path('module', 'admin_views') . '/admin_views.css' => FALSE,
  drupal_get_path('module', 'autocomplete_deluxe') . '/autocomplete_deluxe.css' => FALSE,
  drupal_get_path('module', 'imagefield_focus') . '/imagefield_focus.css' => FALSE,
  drupal_get_path('module', 'date') . '/date_api/date.css' => FALSE,
  drupal_get_path('module', 'ctools') . '/css/button.css' => FALSE,
  drupal_get_path('module', 'ctools') . '/css/dropbutton.css' => FALSE,
  drupal_get_path('module', 'field_group') . '/horizontal-tabs/horizontal-tabs.css' => FALSE,
  drupal_get_path('module', 'inline_entity_form') . '/theme/inline_entity_form.css' => FALSE,
  drupal_get_path('module', 'inline_entity_form') . '/theme/commerce-product.css' => FALSE,
  );

  $css = array_diff_key($css, $exclude);

}

/**
 * Overrides theme_admin_block_content().
 *
 * Use unordered list markup in both compact and extended mode.
 * Source - https://drupal.org/project/shiny
 */
function glance_admin_block_content($variables) {
  $content = $variables['content'];
  $output = '';
  if (!empty($content)) {
    $output = system_admin_compact_mode() ? '<ul class="admin-list compact">' : '<ul class="admin-list">';
    foreach ($content as $item) {
      $output .= '<li class="leaf">';
      $output .= l($item['title'], $item['href'], $item['localized_options']);
      if (isset($item['description']) && !system_admin_compact_mode()) {
        $output .= '<div class="description">' . filter_xss_admin($item['description']) . '</div>';
      }
      $output .= '</li>';
    }
    $output .= '</ul>';
  }
  return $output;
}

/**
 * Override of theme_tablesort_indicator().
 *
 * Use our own image versions, so they show up as black and not gray on gray.
 * Source - https://drupal.org/project/shiny
 */
function glance_tablesort_indicator($variables) {
  $style = $variables['style'];
  $theme_path = drupal_get_path('theme', 'glance');
  if ($style == 'asc') {
    return theme('image', array('path' => $theme_path . '/images/arrow-asc.png', 'alt' => t('sort ascending'), 'width' => 13, 'height' => 13, 'title' => t('sort ascending')));
  }
  else {
    return theme('image', array('path' => $theme_path . '/images/arrow-desc.png', 'alt' => t('sort descending'), 'width' => 13, 'height' => 13, 'title' => t('sort descending')));
  }
}

/**
 * Implements hook_js_alter().
 */

function glance_js_alter(&$js) {
	
	// This file not loads if jquery_update module is enabled
	if(!module_exists('jquery_update')){
		$js['misc/jquery.js']['data'] = drupal_get_path('theme', 'glance') . '/js/jquery-1.7.2.min.js';
	}
	
}


function glance_breadcrumb($variables) {
  $sep = ' <span></span> ';
  if (count($variables['breadcrumb']) > 0) {
    return '<div class="breadcrumb clearfix">' . implode($sep, $variables['breadcrumb']) . '</div>';
  }
  else {
    return t("Home");
  }
}

/**
 * Preprocesses template variables for overlay.tpl.php
 *
 * @see overlay.tpl.php
 */
function glance_preprocess_overlay(&$variables) {
  if (module_exists('crumbs')) {
    $breadcrumb_data = crumbs_get_breadcrumb_data();
    $variables['crumbs_trail'] = $breadcrumb_data['trail'];
    $variables['breadcrumb'] = $breadcrumb_data['html'];
  } else {
    $variables['breadcrumb'] = theme('breadcrumb', array('breadcrumb' => drupal_get_breadcrumb()));
  }
}

function glance_system_info_alter(&$info, $file, $type) {
  if ($type == 'theme') {
    $info['overlay_regions'][] = 'footer';
  }
}

/**
 * Override of theme('node_add_list').
 * Source: Rubik theme
 */
function glance_node_add_list($vars) {
  $content = $vars['content'];

  $output = "<ul class='admin-list'>";
  if ($content) {
    foreach ($content as $item) {
      $item['title'] = "<span class='icon'></span>" . filter_xss_admin($item['title']);
      if (isset($item['localized_options']['attributes']['class'])) {
        $item['localized_options']['attributes']['class'] += _glance_icon_classes($item['href']);
      }
      else {
        $item['localized_options']['attributes']['class'] = _glance_icon_classes($item['href']);
      }
      $item['localized_options']['html'] = TRUE;
      $output .= "<li>";
      $output .= l($item['title'], $item['href'], $item['localized_options']);
      $output .= '<div class="description">'. filter_xss_admin($item['description']) .'</div>';
      $output .= "</li>";
    }
  }
  $output .= "</ul>";
  return $output;
}

/**
 * Generate an icon class from a path.
 * Source: Rubik theme
 */

function _glance_icon_classes($path) {
  $classes = array();
  $args = explode('/', $path);
  if ($args[0] === 'admin' || (count($args) > 1 && $args[0] === 'node' && $args[1] === 'add')) {
    // Add a class specifically for the current path that allows non-cascading
    // style targeting.
    $classes[] = 'path-'. str_replace('/', '-', implode('/', $args)) . '-';
    while (count($args)) {
      $classes[] = drupal_html_class('path-'. str_replace('/', '-', implode('/', $args)));
      array_pop($args);
    }
    return $classes;
  }
  return array();
}

/**
 * Theme the display of the sources list.
 */
function glance_filefield_sources_list($variables) {
  $element = $variables['element'];
  $sources = $variables['sources'];

  $links = array();

  foreach ($sources as $name => $source) {
    $links[] = '<a href="#" onclick="return false;" title="' . $source['description'] . '" id="' . $element['#id'] . '-' . $name . '-source" class="filefield-source filefield-source-' . $name . '">' . $source['label'] . '</a>';
  }
  return '<div class="filefield-sources-list clearfix">' . implode('', $links) . '</div>';
}

function glance_image_widget($variables) {
  $element = $variables['element'];
  $output = '';
  $output .= '<div class="image-widget form-managed-file clearfix">';

  if (isset($element['preview'])) {
    $output .= '<div class="image-preview">';
    $output .= drupal_render($element['preview']);
    $output .= '</div>';
  }

  $output .= '<div class="image-widget-data">';
  if ($element['fid']['#value'] != 0) {
    $element['filename']['#markup'] .= ' <span class="file-size">(' . format_size($element['#file']->filesize) . ')</span> ';
  }
  $output .= drupal_render_children($element);
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

function glance_file_icon($variables) {
  $file = $variables['file'];
	$icon_directory = drupal_get_path('theme', 'glance').'/images/mime';
  $mime = check_plain($file->filemime);
  $icon_url = file_icon_url($file, $icon_directory);
  return '<img class="file-icon" alt="" title="' . $mime . '" src="' . $icon_url . '" />';
}

/**
 * Module: update
 * File: update.report.inc
 */

function glance_update_report($variables) {
  $data = $variables['data'];

  $last = variable_get('update_last_check', 0);
  $output = theme('update_last_check', array('last' => $last));

  if (!is_array($data)) {
    $output .= '<p>' . $data . '</p>';
    return $output;
  }

  $header = array();
  $rows = array();

  $notification_level = variable_get('update_notification_threshold', 'all');

  // Create an array of status values keyed by module or theme name, since
  // we'll need this while generating the report if we have to cross reference
  // anything (e.g. subthemes which have base themes missing an update).
  foreach ($data as $project) {
    foreach ($project['includes'] as $key => $name) {
      $status[$key] = $project['status'];
    }
  }

  foreach ($data as $project) {
    switch ($project['status']) {
      case UPDATE_CURRENT:
        $class = 'ok';
        break;
      case UPDATE_UNKNOWN:
      case UPDATE_FETCH_PENDING:
      case UPDATE_NOT_FETCHED:
        $class = 'unknown';
        break;
      case UPDATE_NOT_SECURE:
      case UPDATE_REVOKED:
      case UPDATE_NOT_SUPPORTED:
        $class = 'error';
        break;
      case UPDATE_NOT_CHECKED:
      case UPDATE_NOT_CURRENT:
      default:
        $class = 'warning';
        break;
    }

    $row = '<div class="version-status">';
    $status_label = theme('update_status_label', array('status' => $project['status']));
    $row .= !empty($status_label) ? $status_label : check_plain($project['reason']);
    //$row .= '<span class="icon"></span>';
    $row .= "</div>\n";

    $row .= '<div class="project">';
    if (isset($project['title'])) {
      if (isset($project['link'])) {
        $row .= l($project['title'], $project['link']);
      }
      else {
        $row .= check_plain($project['title']);
      }
    }
    else {
      $row .= check_plain($project['name']);
    }
    $row .= ' ' . check_plain($project['existing_version']);
    if ($project['install_type'] == 'dev' && !empty($project['datestamp'])) {
      $row .= ' <span class="version-date">(' . format_date($project['datestamp'], 'custom', 'Y-M-d') . ')</span>';
    }
    $row .= "</div>\n";

    $versions_inner = '';
    $security_class = array();
    $version_class = array();
    if (isset($project['recommended'])) {
      if ($project['status'] != UPDATE_CURRENT || $project['existing_version'] !== $project['recommended']) {

        // First, figure out what to recommend.
        // If there's only 1 security update and it has the same version we're
        // recommending, give it the same CSS class as if it was recommended,
        // but don't print out a separate "Recommended" line for this project.
        if (!empty($project['security updates']) && count($project['security updates']) == 1 && $project['security updates'][0]['version'] === $project['recommended']) {
          $security_class[] = 'version-recommended';
          $security_class[] = 'version-recommended-strong';
        }
        else {
          $version_class[] = 'version-recommended';
          // Apply an extra class if we're displaying both a recommended
          // version and anything else for an extra visual hint.
          if ($project['recommended'] !== $project['latest_version']
              || !empty($project['also'])
              || ($project['install_type'] == 'dev'
                && isset($project['dev_version'])
                && $project['latest_version'] !== $project['dev_version']
                && $project['recommended'] !== $project['dev_version'])
              || (isset($project['security updates'][0])
                && $project['recommended'] !== $project['security updates'][0])
              ) {
            $version_class[] = 'version-recommended-strong';
          }
          $versions_inner .= theme('update_version', array('version' => $project['releases'][$project['recommended']], 'tag' => t('Recommended version:'), 'class' => $version_class));
        }

        // Now, print any security updates.
        if (!empty($project['security updates'])) {
          $security_class[] = 'version-security';
          foreach ($project['security updates'] as $security_update) {
            $versions_inner .= theme('update_version', array('version' => $security_update, 'tag' => t('Security update:'), 'class' => $security_class));
          }
        }
      }

      if ($project['recommended'] !== $project['latest_version']) {
        $versions_inner .= theme('update_version', array('version' => $project['releases'][$project['latest_version']], 'tag' => t('Latest version:'), 'class' => array('version-latest')));
      }
      if ($project['install_type'] == 'dev'
          && $project['status'] != UPDATE_CURRENT
          && isset($project['dev_version'])
          && $project['recommended'] !== $project['dev_version']) {
        $versions_inner .= theme('update_version', array('version' => $project['releases'][$project['dev_version']], 'tag' => t('Development version:'), 'class' => array('version-latest')));
      }
    }

    if (isset($project['also'])) {
      foreach ($project['also'] as $also) {
        $versions_inner .= theme('update_version', array('version' => $project['releases'][$also], 'tag' => t('Also available:'), 'class' => array('version-also-available')));
      }
    }

    if (!empty($versions_inner)) {
      $row .= "<div class=\"versions\">\n" . $versions_inner . "</div>\n";
    }
    $row .= "<div class=\"info\">\n";
    if (!empty($project['extra'])) {
      $row .= '<div class="extra">' . "\n";
      foreach ($project['extra'] as $key => $value) {
        $row .= '<div class="' . implode(' ', $value['class']) . '">';
        $row .= check_plain($value['label']) . ': ';
        $row .= drupal_placeholder($value['data']);
        $row .= "</div>\n";
      }
      $row .= "</div>\n";  // extra div.
    }

    $row .= '<div class="includes">';
    sort($project['includes']);
    if (!empty($project['disabled'])) {
      sort($project['disabled']);
      // Make sure we start with a clean slate for each project in the report.
      $includes_items = array();
      $row .= t('Includes:');
      $includes_items[] = t('Enabled: %includes', array('%includes' => implode(', ', $project['includes'])));
      $includes_items[] = t('Disabled: %disabled', array('%disabled' => implode(', ', $project['disabled'])));
      $row .= theme('item_list', array('items' => $includes_items));
    }
    else {
      $row .= t('Includes: %includes', array('%includes' => implode(', ', $project['includes'])));
    }
    $row .= "</div>\n";

    if (!empty($project['base_themes'])) {
      $row .= '<div class="basethemes">';
      asort($project['base_themes']);
      $base_themes = array();
      foreach ($project['base_themes'] as $base_key => $base_theme) {
        switch ($status[$base_key]) {
          case UPDATE_NOT_SECURE:
          case UPDATE_REVOKED:
          case UPDATE_NOT_SUPPORTED:
            $base_themes[] = t('%base_theme (!base_label)', array('%base_theme' => $base_theme, '!base_label' => theme('update_status_label', array('status' => $status[$base_key]))));
            break;

          default:
            $base_themes[] = drupal_placeholder($base_theme);
        }
      }
      $row .= t('Depends on: !basethemes', array('!basethemes' => implode(', ', $base_themes)));
      $row .= "</div>\n";
    }

    if (!empty($project['sub_themes'])) {
      $row .= '<div class="subthemes">';
      sort($project['sub_themes']);
      $row .= t('Required by: %subthemes', array('%subthemes' => implode(', ', $project['sub_themes'])));
      $row .= "</div>\n";
    }

    $row .= "</div>\n"; // info div.

    if (!isset($rows[$project['project_type']])) {
      $rows[$project['project_type']] = array();
    }
    $row_key = isset($project['title']) ? drupal_strtolower($project['title']) : drupal_strtolower($project['name']);
    $rows[$project['project_type']][$row_key] = array(
      'class' => array($class),
      'data' => array($row),
    );
  }

  $project_types = array(
    'core' => t('Drupal core'),
    'module' => t('Modules'),
    'theme' => t('Themes'),
    'module-disabled' => t('Disabled modules'),
    'theme-disabled' => t('Disabled themes'),
  );
  foreach ($project_types as $type_name => $type_label) {
    if (!empty($rows[$type_name])) {
      ksort($rows[$type_name]);
      $output .= "\n<h3>" . $type_label . "</h3>\n";
      $output .= theme('table', array('header' => $header, 'rows' => $rows[$type_name], 'attributes' => array('class' => array('update'))));
    }
  }
  drupal_add_css(drupal_get_path('theme', 'glance') . '/css/modules/update/update.css');
  return $output;
}

function glance_dashboard_regions_alter(&$regions) {
  // Remove the sidebar region defined by the core dashboard module.
  unset($regions['dashboard_sidebar']);
}