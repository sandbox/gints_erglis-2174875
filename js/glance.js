(function ($) {

Drupal.behaviors.glanceBehavior = {
  attach: function (context, settings) {
		
		// Module content_type_extras
		// if Display Suite used for form layout, pre-actions block appears under DS
		// this is quick fix
		$topActions = $('#edit-pre-actions');
		$topActions.parents('.node-form').prepend($topActions);
		
    $(".print-btn").click( function(){
      $(".printable").printElement();
    });
    
  }
};

})(jQuery);
