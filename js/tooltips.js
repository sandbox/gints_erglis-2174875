(function ($) {

Drupal.behaviors.glanceTooltips = {
  attach: function (context, settings) {


	$('.form-item').not('.form-type-radios').each(function (){
		$des = $(this).find('.description');
		$desc = $(this).find('.description').html();
		if($desc > ''){
			$label = $(this).find('label');
			$label.append('<span class="help tooltip">?</span>');
			$label.children().qtip({
				
				content: {
					text: $desc,
				},
				style: {
					classes: 'qtip-bootstrap',
				}
				
			});
			$des.hide();

		}
	});
	
	
  }
};

})(jQuery);
