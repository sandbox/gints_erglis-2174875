<?php
function glance_form_system_theme_settings_alter(&$form, $form_state) {
  $form['glance_tooltips'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Field description tooltips'),
    '#default_value' => theme_get_setting('glance_tooltips'),
    '#description'   => t("If checked, field description shows in tooltip."),
  );
}
?>